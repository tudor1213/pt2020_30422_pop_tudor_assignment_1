package Model;


public class Monomial {
	private int degree;
	private float coeff;

	public Monomial(int degree, float coeff) {
		this.degree = degree;
		this.coeff = coeff;
	}

	public Monomial() {
		this.degree = 0;
		this.coeff = 0;
	}

	public int getDegree() {
		return this.degree;
	}

	public float getCoeff() {
		return this.coeff;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}

	public void setCoeff(float coeff) {
		this.coeff = coeff;
	}

	public void addCoeff(float coeff) {
		this.coeff += coeff;
	}
	
	public void addMonomial(Monomial m2) {
		this.coeff = this.coeff + (float)m2.getCoeff();
	}
	
	public void substractMonomial(Monomial m2){
		this.coeff = this.coeff - (float)m2.getCoeff();
	}

	public Monomial multiplyMonomial(Monomial m) {
		Monomial newMonomial = new Monomial(this.degree + m.getDegree(), (float) ((float) this.coeff * m.getCoeff()));
		return newMonomial;
	}

	public Monomial derivateMonomial() {
		int newDegree = this.degree - 1;
		Monomial newMonomial = new Monomial(newDegree, (float) this.coeff * this.degree);
		return newMonomial;
	}

	public Monomial integrateMonomial() {
		int newDegree = this.degree + 1;
		Monomial newMonomial = new Monomial(newDegree, (float) this.coeff / newDegree);
		return newMonomial;
	}

	public String convertToString() {
		String stringPrinted = new String("+");

		if (this.coeff == Math.round(this.coeff)) {
			stringPrinted = stringPrinted + Integer.toString((int) this.coeff);
		} else {
			stringPrinted = stringPrinted + Float.toString(this.coeff);
		}
		stringPrinted += "x^";
		if (this.degree == Math.round(this.degree)) {
			stringPrinted = stringPrinted + Integer.toString((int) this.degree);
		} else {
			stringPrinted = stringPrinted + Float.toString(this.degree);
		}
    	return stringPrinted;
	}

}