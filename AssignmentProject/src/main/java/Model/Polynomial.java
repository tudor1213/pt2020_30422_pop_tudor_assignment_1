package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polynomial {
    private ArrayList<Monomial>monomList;
    private int degree;
    
    public Polynomial(){
		this.monomList = new ArrayList<Monomial>();
		this.setDegree(0);
    }
    
    public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}
    
    public List<Monomial> getAllMonomials(){
		return this.monomList;
	}
	
	public void setMonomials(ArrayList<Monomial> list){
		this.monomList = list;
		this.setDegree(list.get(0).getDegree());
	}
	
	public void zeroRemove(){
		ArrayList<Monomial> temp = new ArrayList<Monomial>();
		for(Monomial m: this.monomList){
			if(m.getCoeff() == 0f){
				temp.add(m);
			}
		}
		this.monomList.removeAll(temp);
	}
	
	public void sortMonomials() {
		for(Monomial m1: this.monomList) {
			for(Monomial m2: this.monomList) {
				if(m1.getDegree()>m2.getDegree()&& m1!=m2) {
					Monomial aux=new Monomial(m2.getDegree(),m2.getCoeff());
					m2.setDegree(m1.getDegree());
					m2.setCoeff(m1.getCoeff());
					
					m1.setDegree(aux.getDegree());
					m1.setCoeff(aux.getCoeff());
				}	
			}
		}
	}
	
	public void groupAfterDegreeOfMonomials(){
		ArrayList<Monomial> temp = new ArrayList<Monomial>();
		for(Monomial m:this.monomList){
			for(Monomial m1:this.monomList){
				if( m.getDegree() == m1.getDegree() && m != m1){
					
						((Monomial)m).addCoeff(m1.getCoeff());
						if(!temp.contains(m)){
							temp.add(m1);
						}				
					}			
				}
			}
		this.monomList.removeAll(temp);
	}
	
	public void add(Monomial m) {
		this.monomList.add(m);
	}
	
	public String polynomialToText(){
		String textPolynomial = new String("");
		for(Monomial monomial : this.monomList){
			textPolynomial += monomial.convertToString();
		}
		return textPolynomial;
	}
	//Regex
	public Polynomial textToPolynomial(String s) throws WrongInputException {
		Pattern pattern = Pattern.compile("([-+]?\\b\\d+\\.?\\d*)[xX+]\\^(-?\\d+\\b)");
	       Matcher matcher = pattern.matcher(s);
	       ArrayList<Float> vCoeff = new ArrayList<Float>();
	       ArrayList<Integer> vDegree = new ArrayList<Integer>();
	       int matchCondition=0;
	       String matchString = new String();
	       //s = s.replace(" ", "");
	       while (matcher.find()) {
	    	   matchCondition++;
	    	   matchString+="+"+matcher.group(0);
	           Float coeff = Float.valueOf(matcher.group(1));
	           vCoeff.add(coeff);
	           Integer degree = Integer.valueOf(matcher.group(2));
	           vDegree.add(degree);
	       }   
	       if(s.length()+matchCondition==matchString.length()) {   
	       Polynomial p = new Polynomial();
	        for(int i = 0; i < vDegree.size(); i++) {
	    	   Monomial m = new Monomial(vDegree.get(i), vCoeff.get(i));
	    	   p.add(m);
	         }       
	       p.groupAfterDegreeOfMonomials();
	       p.sortMonomials();
	       p.zeroRemove();
	       return p;
	       }
	       else {
	    	   throw new WrongInputException();
	       }
	}
	
	public Polynomial textToPolynomialTest(String s)  {
		Pattern pattern = Pattern.compile("(-?\\b\\d+\\.?\\d*)[xX+]\\^(-?\\d+\\b)");
	       Matcher matcher = pattern.matcher(s);
	       ArrayList<Float> vCoeff = new ArrayList<Float>();
	       ArrayList<Integer> vDegree = new ArrayList<Integer>();
	      // String matchString=new String();
	      // int matchCondition=0;
	       s = s.replace(" ", "");
	       while (matcher.find()) {
	    	  // matchCondition++;
	           Float coeff = Float.valueOf(matcher.group(1));
	           vCoeff.add(coeff);
	           Integer degree = Integer.valueOf(matcher.group(2));
	           vDegree.add(degree);
	        // matchString+="+"+matcher.group(0);
	       }     
	       Polynomial p = new Polynomial();
	        for(int i = 0; i < vDegree.size(); i++) {
	    	   Monomial m = new Monomial(vDegree.get(i), vCoeff.get(i));
	    	   p.add(m);
	         }       
	       p.groupAfterDegreeOfMonomials();
	       p.sortMonomials();
	       p.zeroRemove();
	       //System.out.println("\nmatchString group zero is :"+matchString+" and the length is "+matchString.length());
	      // System.out.println("\n s is :"+s+" and the length is "+s.length());
	       //System.out.println("\n match condition is : \n"+matchCondition);
	        return p;
   }
	
}
	
	
	
	
	
	
	
	
	

