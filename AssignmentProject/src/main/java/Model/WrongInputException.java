package Model;

public class WrongInputException extends Exception {
	public WrongInputException(String message){
		super(message);
	}	
	public WrongInputException(){
		this("One of the inserted polynoms is not valid ");
	}
}

