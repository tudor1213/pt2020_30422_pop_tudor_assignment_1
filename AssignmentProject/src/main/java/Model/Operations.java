package Model;

import java.util.ArrayList;

public class Operations {
	
	public Polynomial addPolynoms( Polynomial p1, Polynomial p2){
		Polynomial result= new Polynomial();
		ArrayList<Monomial> resMonomList= new ArrayList <Monomial>();	
		for(Monomial m1: p1.getAllMonomials()){
			boolean ok= true;
			for(Monomial m2: p2.getAllMonomials()){
				if(m1.getDegree()==m2.getDegree()){
					float newCoeff= m1.getCoeff()+ m2.getCoeff();
					int newDegree=(m1.getDegree());
					Monomial newMonomial=  new Monomial(newDegree,newCoeff);
					resMonomList.add(newMonomial);
					ok= false;
				}
			}
				if(ok== true){
					resMonomList.add(m1);
				}
	    }
		for(Monomial m2: p2.getAllMonomials()){
			boolean ok= true;
			for( Monomial m1: p1.getAllMonomials()){
				if(m1.getDegree()== m2.getDegree()){
					ok= false;
				}
			}
			if(ok==true){
				resMonomList.add(m2);
			}
		}
		result.setMonomials(resMonomList);
		result.groupAfterDegreeOfMonomials();
		result.sortMonomials();
	    result.zeroRemove();
		return result;
	}
	
	public Polynomial substractPolynoms( Polynomial p1, Polynomial p2){
		Polynomial result= new Polynomial();
		ArrayList<Monomial> resMonomList= new ArrayList <Monomial>();
		for(Monomial m1: p1.getAllMonomials()){
			boolean ok= true;
			for(Monomial m2: p2.getAllMonomials()){
				if(m1.getDegree()==m2.getDegree()){
					float newCoeff= m1.getCoeff()- m2.getCoeff();
					int newDegree=(m1.getDegree());
					Monomial newMonomial=  new Monomial(newDegree,newCoeff);
					
					resMonomList.add(newMonomial);
					ok= false;
				}
			}
				if(ok== true){
					resMonomList.add(m1);
				}
			}
		for(Monomial m2: p2.getAllMonomials()){
			boolean ok= true;
			for( Monomial m1: p1.getAllMonomials()){
				if(m1.getDegree()== m2.getDegree()){
					ok= false;
				}
			}
			if(ok==true){
				int newDegree=m2.getDegree();
				float newCoeff=-m2.getCoeff();
				Monomial newMonomial=  new Monomial(newDegree,newCoeff);
				resMonomList.add(newMonomial);
				ok= false;
			}
		}
		result.setMonomials(resMonomList);
		result.groupAfterDegreeOfMonomials();
		result.sortMonomials();
		result.zeroRemove();
		return result;
	}
	
	public Polynomial multiplicationPolynom(Polynomial P1,Polynomial p2){
			Polynomial pResult = new Polynomial();
			for(Monomial m1: P1.getAllMonomials()){
				for(Monomial m2: p2.getAllMonomials()){
					Monomial temp = ((Monomial)m1).multiplyMonomial((Monomial)m2);
					pResult.add(temp);
				}
			}
			pResult.groupAfterDegreeOfMonomials();
			pResult.sortMonomials();
			pResult.zeroRemove();
			return pResult;
		}
	
	public Polynomial derivationPolynom(Polynomial p1){
		Polynomial pResult = new Polynomial();
		for(Monomial m1: p1.getAllMonomials()){
			Monomial temp = (Monomial)m1;
			pResult.add(temp.derivateMonomial());
		}
		pResult.groupAfterDegreeOfMonomials();
		pResult.sortMonomials();
		pResult.zeroRemove();
		return pResult;
	}
	
	public Polynomial integrationPolynom(Polynomial p1){
		Polynomial pResult = new Polynomial();
		for(Monomial m1: p1.getAllMonomials()){
			Monomial temp = (Monomial)m1;
			pResult.add(temp.integrateMonomial());
		}
		pResult.groupAfterDegreeOfMonomials();
		pResult.sortMonomials();
		pResult.zeroRemove();
		return pResult;
	}
	
	
	
	
	
	
	
	
	
	
}
