package Control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import Model.Operations;
import Model.Polynomial;
import Model.WrongInputException;
import View.View;

public class Controller {
	 View view;
	public Controller(){
		this.view = new View();
		this.view.addListenerjAdd(new ListenerjAdd());
		this.view.addListenerjSub(new ListenerjSub());
		this.view.addListenerjMul(new ListenerjMultiplication());
		this.view.addListenerjDiv(new ListenerjDivision());
		this.view.addListenerjInt1(new ListenerjIntegral1());
		this.view.addListenerjDer1(new ListenerjDerivative1());
		this.view.addListenerjInt2(new ListenerjIntegral2());
		this.view.addListenerjDer2(new ListenerjDerivative2());
		this.view.addListenerjReset(new ListenerjReset());
	}
public class ListenerjAdd implements ActionListener{
	  public void actionPerformed(ActionEvent e) {
		Polynomial ResAdd= new Polynomial();
	     Polynomial P1Add= new Polynomial();
	     Polynomial P2Add= new Polynomial();
	     String answerAdd; 
	     Operations operation= new Operations();
	     String S1Add=view.getTextPolynomField1().getText();
	     String S2Add=view.getTextPolynomField2().getText(); 
	     try{
	    	 P1Add=P1Add.textToPolynomial(S1Add);
		     P2Add=P2Add.textToPolynomial(S2Add);
			}
			catch (WrongInputException exAdd){
				JOptionPane.showMessageDialog(view.frame, exAdd.getMessage(), "Wrong input", JOptionPane.WARNING_MESSAGE);
			}			
	      
	       ResAdd=operation.addPolynoms(P1Add,P2Add); 
	       answerAdd=ResAdd.polynomialToText();
           view.getTextResultField().setText(answerAdd);	
	 }
}

public class ListenerjSub implements ActionListener{
	  public void actionPerformed(ActionEvent e) {
		  Polynomial ResSub=new Polynomial();
		     Polynomial P1Sub= new Polynomial();
		     Polynomial P2Sub= new Polynomial();
		     String answerSub; 
		     Operations operation= new Operations();
		     String S1Sub=view.getTextPolynomField1().getText();
		     String S2Sub=view.getTextPolynomField2().getText(); 
		       try{
		    	   P1Sub=P1Sub.textToPolynomial(S1Sub);
			       P2Sub=P2Sub.textToPolynomial(S2Sub);
				}
				catch (WrongInputException exSub){
					JOptionPane.showMessageDialog(view.frame, exSub.getMessage(), "Wrong input", JOptionPane.WARNING_MESSAGE);
				}	
		       ResSub=operation.substractPolynoms(P1Sub,P2Sub); 
		       answerSub=ResSub.polynomialToText();;
	           view.getTextResultField().setText(answerSub); 
	 }
}

public class ListenerjMultiplication implements ActionListener{
	  public void actionPerformed(ActionEvent e) {
		     Polynomial ResMul=new Polynomial();
		     Polynomial P1Mul= new Polynomial();
		     Polynomial P2Mul= new Polynomial();
		     String answerMul; 
		     Operations operation= new Operations();
		     String S1Mul=view.getTextPolynomField1().getText();
		     String S2Mul=view.getTextPolynomField2().getText();
		     try{
		    	   P1Mul=P1Mul.textToPolynomial(S1Mul);
			       P2Mul=P2Mul.textToPolynomial(S2Mul);
				}
				catch (WrongInputException exMul){
					JOptionPane.showMessageDialog(view.frame, exMul.getMessage(), "Wrong input", JOptionPane.WARNING_MESSAGE);
				}
		       ResMul=operation.multiplicationPolynom(P1Mul,P2Mul); 
		       answerMul=ResMul.polynomialToText();;
	           view.getTextResultField().setText(answerMul);
	 }
}
public class ListenerjDivision implements ActionListener{	
	 public void actionPerformed(ActionEvent e) {
		 JOptionPane.showMessageDialog(view.frame, "This operation is not implemented yet", "Not implemented", JOptionPane.WARNING_MESSAGE);
	 }	
}

public class ListenerjIntegral1 implements ActionListener{	
	 public void actionPerformed(ActionEvent e) {
		 Polynomial Res1Int=new Polynomial();
	     Polynomial P1Int= new Polynomial();
	     String answer1Int; 
	     Operations operation= new Operations();
	     String S1Int=view.getTextPolynomField1().getText(); 
	     try{
	    	 P1Int=P1Int.textToPolynomial(S1Int);
			}
			catch (WrongInputException exInt1){
				JOptionPane.showMessageDialog(view.frame, "Polynom1 is invalid", "Wrong input", JOptionPane.WARNING_MESSAGE);
			}
	       Res1Int=operation.integrationPolynom(P1Int); 
	       answer1Int=Res1Int.polynomialToText();;
           view.getTextResultField().setText(answer1Int);
	 }	
}

public class ListenerjDerivative1 implements ActionListener{	
	 public void actionPerformed(ActionEvent e) {
		 Polynomial Res1Der=new Polynomial();
	     Polynomial P1Der= new Polynomial();
	     String answer1Der; 
	     Operations operation= new Operations();
	     String S1Der=view.getTextPolynomField1().getText();
	     try{
	    	 P1Der=P1Der.textToPolynomial(S1Der);
			}
			catch (WrongInputException exDer1){
				JOptionPane.showMessageDialog(view.frame, "Polynom1 is invalid", "Wrong input", JOptionPane.WARNING_MESSAGE);
			}
	       Res1Der=operation.derivationPolynom(P1Der); 
	       answer1Der=Res1Der.polynomialToText();;
           view.getTextResultField().setText(answer1Der);
	 }
}

public class ListenerjIntegral2 implements ActionListener{	
	 public void actionPerformed(ActionEvent e) {
		 Polynomial Res2Int=new Polynomial();
	     Polynomial P2Int= new Polynomial();
	     String answer2Int; 
	     Operations operation= new Operations();
	     String S2Int=view.getTextPolynomField2().getText(); 
	     try{
	    	 P2Int=P2Int.textToPolynomial(S2Int);
			}
			catch (WrongInputException exInt1){
				JOptionPane.showMessageDialog(view.frame, "Polynom2 is invalid", "Wrong input", JOptionPane.WARNING_MESSAGE);
			}
	       Res2Int=operation.integrationPolynom(P2Int); 
	       answer2Int=Res2Int.polynomialToText();;
          view.getTextResultField().setText(answer2Int);
	 }	
}

public class ListenerjDerivative2 implements ActionListener{	
	 public void actionPerformed(ActionEvent e) {
		 Polynomial Res2Der=new Polynomial();
	     Polynomial P2Der= new Polynomial();
	     String answer2Der; 
	     Operations operation= new Operations();
	     String S2Der=view.getTextPolynomField2().getText();
	     try{
	    	 P2Der=P2Der.textToPolynomial(S2Der);
			}
			catch (WrongInputException exDer2){
				JOptionPane.showMessageDialog(view.frame,"Polynom2 is invalid", "Wrong input", JOptionPane.WARNING_MESSAGE);
			}
	       Res2Der=operation.derivationPolynom(P2Der); 
	       answer2Der=Res2Der.polynomialToText();;
          view.getTextResultField().setText(answer2Der);
	 }
}
public class ListenerjReset implements ActionListener{	
     public void actionPerformed(ActionEvent e) {                                       
        view.getTextPolynomField1().setText("") ; 
        view.getTextPolynomField2().setText("") ;
        view.getTextResultField().setText("") ;
     } 
}





























}
	 
	 
	 
	 
	 
	 
	 
	 
	 