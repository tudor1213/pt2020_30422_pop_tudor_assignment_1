package View;

import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class View {
	// Variables declaration - do not modify
	public JFrame frame;
    private javax.swing.JButton jAdd;
    private javax.swing.JButton jDerivative1;
    private javax.swing.JButton jDerivative2;
    private javax.swing.JButton jDivision;
    private javax.swing.JButton jIntegral1;
    private javax.swing.JButton jIntegral2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JButton jMultiplication;
    private javax.swing.JLabel jPolyExample;
    private javax.swing.JTextField jPolynomField1;
    private javax.swing.JTextField jPolynomField2;
    private javax.swing.JButton jReset;
    private javax.swing.JLabel jResult;
    private javax.swing.JTextField jResultField;
    private javax.swing.JButton jSubstract;
    // End of variables declaration   
	
    public View() {
        initComponents();
    }
    private void initComponents() {
    	frame = new JFrame();
        jPolynomField1 = new javax.swing.JTextField();
        jMultiplication = new javax.swing.JButton();
        jSubstract = new javax.swing.JButton();
        jDivision = new javax.swing.JButton();
        jAdd = new javax.swing.JButton();
        jPolynomField2 = new javax.swing.JTextField();
        jReset = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jIntegral1 = new javax.swing.JButton();
        jDerivative1 = new javax.swing.JButton();
        jIntegral2 = new javax.swing.JButton();
        jDerivative2 = new javax.swing.JButton();
        jResultField = new javax.swing.JTextField();
        jResult = new javax.swing.JLabel();
        jPolyExample = new javax.swing.JLabel();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //buttons
        jAdd.setText("ADD");
        jSubstract.setText("SUBSTRACT");
        jMultiplication.setText("MULTIPLICATION");
        jDivision.setText("DIVISION");
        jIntegral1.setText("Integral1");
        jDerivative1.setText("Derivative1");
        jIntegral2.setText("Integral2");
        jDerivative2.setText("Derivative2");
        jReset.setText("RESET");
        //labels
        frame.setTitle("Polynomial Calculator");
        jResult.setText("RESULT");
        jLabel1.setText("Polynom1");
        jLabel2.setText("Polynom2");     
        jPolyExample.setText("Polynom Input Example: \"-5x^3+4x^2-3x^1+9x^0\"");
        jPolyExample.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.getAccessibleContext().setAccessibleName("jPolynom1");
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(frame.getContentPane());
        frame.getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE)
                    .addComponent(jResult, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jResultField)
                        .addGap(18, 18, 18)
                        .addComponent(jReset, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jPolynomField2, javax.swing.GroupLayout.PREFERRED_SIZE, 367, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 95, Short.MAX_VALUE)
                        .addComponent(jIntegral2))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jPolynomField1, javax.swing.GroupLayout.PREFERRED_SIZE, 367, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jIntegral1)))
                .addGap(73, 73, 73)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDerivative1)
                    .addComponent(jDerivative2))
                .addGap(85, 85, 85))
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jAdd)
                .addGap(30, 30, 30)
                .addComponent(jSubstract)
                .addGap(38, 38, 38)
                .addComponent(jDivision)
                .addGap(34, 34, 34)
                .addComponent(jMultiplication)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPolyExample, javax.swing.GroupLayout.PREFERRED_SIZE, 443, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPolyExample, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jPolynomField1, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jIntegral1, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                            .addComponent(jDerivative1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(91, 91, 91)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jPolynomField2, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jIntegral2, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jDerivative2, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(142, 142, 142)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jDivision, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSubstract, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jMultiplication, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(83, 83, 83)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jReset, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jResultField, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jResult, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(113, 113, 113))
        );
        frame.pack();
    }// </editor-fold>                        
    
    public JTextField getTextPolynomField1() {
		return jPolynomField1;
	}
    
    public JTextField getTextPolynomField2() {
		return jPolynomField2;
	}
    
    public JTextField getTextResultField() {
		return jResultField;
	}
    
    public void addListenerjAdd( ActionListener button){
		this.jAdd.addActionListener( button);
	}
    
    public void addListenerjSub( ActionListener button){
		this.jSubstract.addActionListener( button);
	}
    
    public void addListenerjMul( ActionListener button){
		this.jMultiplication.addActionListener( button);
	}
    
    public void addListenerjDiv( ActionListener button){
		this.jDivision.addActionListener( button);
	}
    
    public void addListenerjInt1( ActionListener button){
		this.jIntegral1.addActionListener( button);
	}
    
    public void addListenerjDer1( ActionListener button){
		this.jDerivative1.addActionListener( button);
	}
    
    public void addListenerjInt2( ActionListener button){
		this.jIntegral2.addActionListener( button);
	}
  
    public void addListenerjDer2( ActionListener button){
		this.jDerivative2.addActionListener( button);
	}
    
    public void addListenerjReset( ActionListener button){
		this.jReset.addActionListener( button);
	}    
}