package TestPackage;
import Model.*;

import static org.junit.Assert.*;
import org.junit.Test;


public class Tests {
	
    @Test
	public void testAdd(){
		 Polynomial resAdd=new Polynomial();
	     Polynomial p1Add= new Polynomial();
	     Polynomial p2Add= new Polynomial();
	     String s1Add="2x^3+3x^2-4x^1-4x^0";
	     String s2Add="2x^3-3x^2-2x^1";
	     String answerAdd; 
	     Operations operation= new Operations();
	       p1Add=p1Add.textToPolynomialTest(s1Add);
 	       p2Add=p2Add.textToPolynomialTest(s2Add);
	       resAdd=operation.addPolynoms(p1Add,p2Add); 
	       answerAdd=resAdd.polynomialToText();
	       //System.out.println("\nAddition result is : \n"+answerAdd);
	       assertEquals("+4x^3+-6x^1+-4x^0", answerAdd);
	       assertNotEquals("+4x^3+-6x^1+-4x^0",answerAdd);     
	}
    
    @Test
   	public void testSub(){
   		 Polynomial resSub=new Polynomial();
   	     Polynomial p1Sub= new Polynomial();
   	     Polynomial p2Sub= new Polynomial();
   	     String s1Sub="3x^3+4x^2-4x^1";
   	     String s2Sub="2x^3+3x^2-2x^1+5x^0";
   	     String answerSub; 
   	     Operations operation= new Operations();
   	       p1Sub=p1Sub.textToPolynomialTest(s1Sub);
   	       p2Sub=p2Sub.textToPolynomialTest(s2Sub);
   	       resSub=operation.substractPolynoms(p1Sub,p2Sub); 
   	       answerSub=resSub.polynomialToText();
   	     // System.out.println("\nSubstraction result is : \n"+ answerSub);
   	       assertEquals("+1x^3+1x^2+-2x^1+-5x^0", answerSub);
   	       assertNotEquals("+1x^3+-6x^1+-4x^0",answerSub);
   	}
    
    @Test
   	public void testMultiplication(){
   		 Polynomial resMul=new Polynomial();
   	     Polynomial p1Mul= new Polynomial();
   	     Polynomial p2Mul= new Polynomial();
   	     
   	     String s1Mul="-5x^3+1x^2";
   	     String s2Mul="1x^20-1x^0";
   	     String answerMul; 
   	     Operations operation= new Operations();
   	     
   	       p1Mul=p1Mul.textToPolynomialTest(s1Mul);
   	       p2Mul=p2Mul.textToPolynomialTest(s2Mul);
   	       resMul=operation.multiplicationPolynom(p1Mul,p2Mul) ;
   	       answerMul=resMul.polynomialToText();
   	      // System.out.println("\nMultiplication result is : \n"+answerMul);
   	       
   	       assertEquals("+-5x^23+1x^22+5x^3+-1x^2", answerMul);
   	       assertNotEquals("+-5x^23+1x^22+5x^3+-1x^2", answerMul);
   	}
	
    @Test
    public void testIntegration(){
  		 Polynomial resInt=new Polynomial();
  	     Polynomial p1Int= new Polynomial();    
  	     String s1Int="-12x^3+2x^1";
  	     String answerInt; 
  	     Operations operation= new Operations();
  	     
  	       p1Int=p1Int.textToPolynomialTest(s1Int);
  	       resInt=operation.integrationPolynom(p1Int) ;
  	       answerInt=resInt.polynomialToText();
  	       //System.out.println("\nIntegration result is : \n"+answerInt);
  	       
  	       assertEquals("+-3x^4+1x^2", answerInt);
  	       assertNotEquals("+-2x^4+1x^2", answerInt);
  	}
   
    @Test
    public void testDerivation(){
  		 Polynomial resDer=new Polynomial();
  	     Polynomial p1Der= new Polynomial();    
  	     String s1Der="-12x^3-22x^1-3x^0";
  	     String answerDer; 
  	     Operations operation= new Operations();
  	     
  	       p1Der=p1Der.textToPolynomialTest(s1Der);
  	       resDer=operation.derivationPolynom(p1Der) ;
  	       answerDer=resDer.polynomialToText();
  	       //System.out.println("\nDerivation result is : \n"+answerDer);
  	       
  	       assertEquals("+-36x^2+-22x^0", answerDer);
  	       assertNotEquals("+-35x^2+2x^0", answerDer);
  	      
    }
    
    
    
    
    
}
